# QSparse Library

Temporary Repository For AISTAT 2022 Anonymous Submission id: 2056

examples for using this library: 



```python

from torch import nn
from qsparse import quantize, prune

def create_p_q(epoch_size):

    def q(*args, c=0):
		return quantize(
				timeout=epoch_size *  8,
				channelwise=-1
			)
			if len(args) == 0
			else quantize(
				args[0],
				timeout=epoch_size * 7,
				channelwise=c or 1,
			)

    def p(*args):
        kw = {
            "start": epoch_size * 2,
            "interval": epoch_size * 1,
            "repetition": 3,
            "sparsity": 0.5,
        }
		return prune(**kw, window_size=bs) if len(args) == 0 else prune(args[0], **kw)

    return p, q

class Net(nn.Module):
    def __init__(self, epoch_size=-1):
        super(Net, self).__init__()
        p, q = create_p_q(epoch_size)

        self.qin = q()

        self.conv1 = q(nn.Conv2d(1, 32, 3, 1))
        self.bn1 = nn.BatchNorm2d(32)
        self.p1, self.q1 = p(), q()

        self.conv2 = q(p(nn.Conv2d(32, 64, 3, 1)))
        self.bn2 = nn.BatchNorm2d(64)
        self.p2, self.q2 = p(), q()

        self.fc1 = q(p(nn.Linear(9216, 128)), c=-1)
        self.bn3 = nn.BatchNorm1d(128)
        self.p3, self.q3 = p(), q()

        self.fc2 = q(nn.Linear(128, 10), c=-1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)

    def forward(self, x):
        x = self.qin(x)
        x = F.relu(self.q1(self.p1(self.bn1(self.conv1(x)))))
        x = F.relu(self.q2(self.p2(self.bn2(self.conv2(x)))))
        x = F.max_pool2d(x, 2)
        x = self.dropout1(x)
        x = torch.flatten(x, 1)
        x = F.relu(self.q3(self.p3(self.bn3(self.fc1(x)))))
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output
```

